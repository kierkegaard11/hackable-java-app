package ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.Controller;
import model.User;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class FrmMain extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsername;
	private JPasswordField txtPassword;
	private JTextArea txtResult;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public FrmMain() {
		setTitle("Log In");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 398);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(90, 76, 108, 15);
		contentPane.add(lblUsername);

		txtUsername = new JTextField();
		txtUsername.setBounds(233, 74, 175, 19);
		contentPane.add(txtUsername);
		txtUsername.setColumns(10);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(90, 122, 70, 15);
		contentPane.add(lblPassword);

		txtPassword = new JPasswordField();
		txtPassword.setBounds(233, 120, 175, 19);
		contentPane.add(txtPassword);

		JButton btnLogin = new JButton("Log In");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				login();
				//login2();
				//login3();
			}

		});
		btnLogin.setBounds(179, 186, 117, 25);
		contentPane.add(btnLogin);

		txtResult = new JTextArea();
		txtResult.setEditable(false);
		txtResult.setBounds(12, 223, 466, 133);
		contentPane.add(txtResult);
	}

	private void login() {
		txtResult.setText("");
		String username = txtUsername.getText();
		String password = new String(txtPassword.getPassword());
		Controller controller = Controller.getInstance();
		User user;
		try {
			user = controller.login(username, password);
			txtResult.setText("First name: " + user.getFirstName() + " Last name: " + user.getLastName());
		} catch (Exception e) {
			txtResult.setText("Wrong credentials!");
		}
	}

	private void login2() {
		txtResult.setText("");
		String username = txtUsername.getText();
		String password = new String(txtPassword.getPassword());
		Controller controller = Controller.getInstance();
		List<User> users = new ArrayList<>();
		try {
			users = controller.login2(username, password);
			String text = "";
			for (User user : users) {
				text += "First name: ";
				text += user.getFirstName();
				text += " Last name: ";
				text += user.getLastName();
				text += "\n";
				txtResult.append(text);
			}

			System.out.println(text);
		} catch (Exception e) {
			txtResult.setText("Wrong credentials!");
		}
	}

	private void login3() {
		txtResult.setText("");
		String username = txtUsername.getText().trim();
		String password = new String(txtPassword.getPassword());
		Pattern pattern = Pattern.compile("[a-zA-Z0-9]*", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(username);
		if (!matcher.matches()) {
			txtResult.setText("Username contains only letters and numbers!");
		} else {
			Controller controller = Controller.getInstance();
			User user;
			try {
				user = controller.login(username, password);
				txtResult.setText("First name: " + user.getFirstName() + " Last name: " + user.getLastName());
			} catch (Exception e) {
				txtResult.setText("Wrong credentials!");
			}
		}
	}
}

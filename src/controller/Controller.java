package controller;

import java.util.List;

import db.DBBroker;
import model.User;

public class Controller {

	private static Controller instance;

	public static Controller getInstance() {
		if (instance == null) {
			instance = new Controller();
		}
		return instance;
	}

	public User login(String username, String password) throws Exception {
		DBBroker dbBroker = DBBroker.getInstance();
		User user = dbBroker.findUserByUsernameAndPassword(username, password);
		//User user = dbBroker.findUserByUsernameAndPasswordPrepared(username, password);
		if (user==null) {
			throw new Exception("Bad credentials!");
		}
		return user;
	}

	public List<User> login2(String username, String password) throws Exception {
		DBBroker dbBroker = DBBroker.getInstance();
		List<User> users = dbBroker.findUsersByUsernameAndPassword(username, password);
		if (users.isEmpty()) {
			throw new Exception("Bad credentials!");
		}
		return users;
	}

}

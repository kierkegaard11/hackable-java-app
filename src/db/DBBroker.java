package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.User;

public class DBBroker {

	private Connection connection = null;
	private String driver;
	private String url;
	private String dbusername;
	private String dbpassword;
	private String dbName;
	private static DBBroker instance;

	private DBBroker() {
		makeConnection();
	}

	public static DBBroker getInstance() {
		if (instance == null) {
			instance = new DBBroker();
		}
		return instance;
	}

	public Connection getConnection() {
		return connection;
	}

	public boolean makeConnection() {
		readConfigProperties();
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url, dbusername, dbpassword);
			connection.setAutoCommit(false);
		} catch (Exception ex) {
			System.out.println(ex);
			return false;
		}
		return true;
	}

	void readConfigProperties() {
		driver = "com.mysql.cj.jdbc.Driver";
		dbName = "test";
		url = "jdbc:mysql://127.0.0.1:3306/" + dbName;
		dbusername = "root";
		dbpassword = "root";
	}

	public User findUserByUsernameAndPassword(String username, String password) {
		ResultSet resultSet = null;
		Statement statement = null;
		User user = null;
		String query = "select * from user where username='" + username + "' and password='" + password + "'";
		System.out.println(query);
		boolean signal;
		try {
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery(query);
			signal = resultSet.next();
			if (signal == true) {
				user = getNewRecord(resultSet);
			}
		} catch (Exception ex) {
			System.out.println(ex);
		} finally {
			close(null, statement, resultSet);
		}

		return user;
	}

	private User getNewRecord(ResultSet rs) throws SQLException {
		return new User(rs.getInt("id"), rs.getString("username"), rs.getString("password"), rs.getString("first_name"),
				rs.getString("last_name"));
	}

	public void close(Connection connection, Statement statement, ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException ex) {
				Logger.getLogger(DBBroker.class.getName()).log(Level.SEVERE, null, ex);
			}
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException ex) {
				Logger.getLogger(DBBroker.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException ex) {
				Logger.getLogger(DBBroker.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	public List<User> findUsersByUsernameAndPassword(String username, String password) {
		ResultSet resultSet = null;
		Statement statement = null;
		List<User> users = new ArrayList<>();
		String query = "select * from user where username='" + username + "' and password='" + password + "'";
		System.out.println(query);
		try {
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery(query);

			while (resultSet.next()) {
				User user = getNewRecord(resultSet);
				users.add(user);
			}
		} catch (SQLException ex) {
			Logger.getLogger(DBBroker.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			close(null, statement, resultSet);
		}
		return users;
	}

	public User findUserByUsernameAndPasswordPrepared(String username, String password) {
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		User user = null;
		String query = "select * from user where username=? and password=?";

		boolean signal;
		try {
			statement = connection.prepareStatement(query);
			statement.setString(1, username);
			statement.setString(2, password);
			resultSet = statement.executeQuery();
			signal = resultSet.next();
			if (signal == true) {
				user = getNewRecord(resultSet);
			}
		} catch (Exception ex) {
			System.out.println(ex);
		} finally {
			close(null, statement, resultSet);
		}

		return user;
	}

}

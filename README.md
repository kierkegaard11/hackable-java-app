# Hackable Java App

Ovo je mini klijent-server aplikacija napisana u Javi koja ima određene
ranjivosti. Namenjena je za prvi čas vežbi iz predmeta Zaštita računarskih sistema.

Kako da importujete projekat?

### Možete koristiti JDK 8+.

## MYSQL
Instalirajte MySQL server i zatim ga pokrenite.  

ILI  

Instalirajte Docker, a zatim pokrenite  

`docker run -d --name mysqldemo -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root mysql:latest`.  
Možete koristiti bilo koji MySQL klijent (Workbench, DataGrip itd.).  
Napravite šemu sa nazivom `test` i importujte fajl `test.sql` koji se nalazi na lokaciji `src/resources/test.sql`.   
U aplikaciji je podešeno sledeće:  
MySQL port 3306,  
username: root  
password: root  
database name: test  
Ukoliko ste menjali neki od parametara u konekciji, promenite ih
i u aplikaciji da bi radila.  


## Import projekta
### Eclipse i Apache NetBeans
Ukoliko ovaj projekat importujete u Eclipse ili Apache NetBeans IDE ne bi trebalo da imate bilo
kakve probleme. Potrebno je samo da preuzmete MySQL JDBC Connector (ukoliko ga nemate)
i dodate .jar fajl u projekat.
https://dev.mysql.com/downloads/connector/j/

### IntelliJ

U ovom IDE-u možete na poseban način uvesti projekte pisane u Eclipse-u
i to na sledeći način: `File/New/Project from existing sources`.  
Odaberite projekat, zatim `Import project from external model`, pa `Eclipse`.  
Ove opcije se mogu razlikovati u zavisnosti od verzije IntelliJ-a.  
Preuzmite MySQL JDBC connector (https://dev.mysql.com/downloads/connector/j/) i dodajte ga u `File/Project Structure/Libraries`.  
Trebalo bi da se .jar fajl nalazi na lokaciji `/usr/share/java`.  

